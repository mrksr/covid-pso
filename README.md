# Covid-PSO

Covid-pso is a Python project that interfaces with a local fork [CovidSIM](http://covidsim.exploratory.systems/) to find simulation parameters using Particle Swarm Optimization (PSO).
We use it to find simulation parameters that explain data observed in the real world.

Dependencies:
* [**mrksr/python-pso**](https://gitlab.com/mrksr/python-pso): A simple PSO implementation in Python.
* [**fstrixner/covidsim**](https://gitlab.com/fstrixner/covidsim): A fork of CovidSIM that exposes a REST API and is node.js compatible.

## Usage
This project uses [poetry](https://python-poetry.org/), a packaging and dependency management system for Python.
To setup your development environment, clone the repository and run
```
$ poetry install
```

The code in this repository assumes that you start your instances of covidsim yourself. Take a look at [`fstrixner/covidsim`](https://gitlab.com/fstrixner/covidsim) for a tutorial on how to set it up. To speed things up, some of the notebook parallelize along multiple instances of the simulator. The corresponding code in the notebooks is quite straightforward.

The package itself only contains some glue code for the simulator.
The paramter search is performed in the notebooks in the `notebooks` directory.
Take a look at [`notebooks/04_korea_visualization.ipynb`](notebooks/04_korea_visualization.ipynb) for some results.
