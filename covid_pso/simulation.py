import json

import aiohttp
import pandas as pd
import requests

SIMULATION_URL = "http://localhost:3000"
DEFAULT_PARAMETERS = {
    # Population
    "population_size": 100.0,
    "initial_cases": 1.0,
    "external_force_of_infection": 1.0,
    "simulation_days": 365.0,
    # Durations
    "latency_duration": 4.0,
    "prodromal_duration": 1.0,
    "infective_duration": 10.0,
    "convalescent_duration": 0.0,
    "latency_stages": 16.0,
    "prodromal_stages": 16.0,
    "infective_stages": 16.0,
    "convalescent_stages": 0.0,
    "erlang_stages": 16.0,
    # Severity
    "symptomatic_course_of_disease": 58.0,
    "medical_consultations_by_symptomatic_cases": 40.0,
    "hospitalisation_of_symptomatic_cases": 2.0,
    "icu_patients_among_hospitalized_cases": 25.0,
    "case_fatality_of_symptomatic_cases": 2.0,
    # Contagiousness
    "basic_reproduction_number": 4.0,
    "maximal_seasonal_transmission_factor": 0.0,
    "maximal_seasonal_transmission_day": 0.0,
    "prodromal_contagiousness": 50.0,
    "convalescent_contagiousness": 100.0,
    # Case Isolation
    "symptomatic_cases_to_be_isolated": 0.0,
    "case_isolation_capacity": 0.0,
    "contact_reduction_home_isolation": 0.0,
    "case_isolation_begin": 0.0,
    "case_isolation_range": 0.0,
    # General Contact Reduction
    "contact_reduction_factor": 0.0,
    "contact_reduction_begin": 0.0,
    "contact_reduction_range": 0.0,
    # Triggered General Contact Reduction
    "sick_threshold": 0.0,
    "sick_triggered_contact_reduction": 0.0,
    "hospitalisation_threshold": 0.0,
    "hospitalisation_triggered_contact_reduction": 0.0,
    "icu_threshold": 0.0,
    "icu_triggered_contact_reduction": 0.0,
    # Detection
    "detection_probability_medical_consultation": 10.0,
    "detection_probability_hospitalisation": 10.0,
    "detection_probability_deceased_case": 10.0,
}


def simulate(parameters, url=SIMULATION_URL):
    headers = {"Content-type": "application/json", "Accept": "text/plain"}
    response = requests.post(url, data=json.dumps(parameters), headers=headers)
    return response.json()


async def simulate_async(parameters, url=SIMULATION_URL):
    headers = {'Content-type': 'application/json', 'Accept': 'text/plain'}
    async with aiohttp.ClientSession() as session:
        async with session.post(
            url, data=json.dumps(parameters), headers=headers
        ) as response:
            return await response.json()


def parse_result(json_result, desired_keys=None):
    # NOTE(mrksr): The response JSON contains one key per timeseries. We parse
    # them in turn and concat them to a single dataframe.
    def parse_single_timeseries(timeseries, key):
        df = pd.DataFrame(timeseries["values"])
        df = df.rename(columns={"x": "days", "y": key})
        df = df.set_index("days")
        return df

    if desired_keys is None:
        desired_keys = json_result.keys()

    return pd.concat(
        [
            parse_single_timeseries(timeseries, key)
            for key, timeseries in json_result.items()
            if isinstance(timeseries, dict)
            if key in desired_keys
        ],
        axis=1,
    )
